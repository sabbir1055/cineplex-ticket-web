import React from 'react'
import ReactDom from 'react-dom'
import App from './App'
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';
import './assets/assets/navmenu/bootsnav';
import './assets/js/bootstrap-datetimepicker';
import './assets/js/stickyBar'
import './assets/js/main.js';
import './assets/css/all.min.css';
import './assets/css/custom.css';

ReactDom.render(<App />, document.getElementById('root'))