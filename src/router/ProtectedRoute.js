import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
const ProtectedRoute = ({ component: Component, ...rest }) => {
  const user = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null
  
  return (
    user ?
      <Route {...rest} render={
        props => <Component {...rest} {...props} />
      } />
      :
      <Redirect to="/login" />
  )
}

export default ProtectedRoute;