import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute'
import GuestRoute from './GuestRoute'
import Login from '../components/Login'
import Register from '../components/Register'
import TicketBoard from '../components/TicketBoard'
import NotFound from '../components/NotFound'

const Router = () => (
    <BrowserRouter>
        <Switch>
            <GuestRoute exact path="/" render={(props) => (
                props.history.push('/login')
            )} />
            <GuestRoute exact path="/login" component={Login} />
            <GuestRoute exact path="/register" component={Register} />
            <ProtectedRoute exact path='/home' component={TicketBoard} />
            <Route component={NotFound} />
        </Switch>
    </BrowserRouter>
);
export default Router;