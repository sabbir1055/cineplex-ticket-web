import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
const GuestRoute = ({ component: Component, ...rest }) => {
    const user = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null
    return (
        user ?
            <Redirect to="/home" />
            :
            <Route {...rest} render={
                props => <Component {...rest} {...props} />
            } />
    )
}

export default GuestRoute;