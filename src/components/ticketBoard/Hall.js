import React from 'react'

export default function Hall() {
    return (
        <div>
            <div className="ticket_booking_left_item movie_show_time">
                                    <h3>Select Show Time</h3>
                                    <div className="card-wrap">
                                        <div className="showtime-list-view">
                                            <p>Hall No.</p>
                                            <h3>Hall 01</h3>
                                        </div>
                                        <ul className="showtime-each-view">
                                            <li className="selected">
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="card-wrap">
                                        <div className="showtime-list-view">
                                            <p>Hall No.</p>
                                            <h3>Hall 02</h3>
                                        </div>
                                        <ul className="showtime-each-view">
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="card-wrap">
                                        <div className="showtime-list-view">
                                            <p>Hall No.</p>
                                            <h3>Hall 03</h3>
                                        </div>
                                        <ul className="showtime-each-view">
                                            <li className="">
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                            <li>
                                                <a href="#"> 1:25 PM</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                                
        </div>
    )
}
