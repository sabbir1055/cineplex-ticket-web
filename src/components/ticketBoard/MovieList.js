import React from 'react'
import Spiderman from '../../assets/images/icons/selected.svg'
import poster from '../../assets/images/icons/poster.png'
export default function MovieList() {
    return (
        <div>
            <div className="ticket_booking_left_item movie_list_show">
                <h3>Select Movie (10)</h3>
                <ul className="d-flex movie_list_item">
                    <li className="selected">
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                    <li>
                        <img src={poster} alt="" />
                        <p>Spider-Man: Far From Home</p>
                        <div className="if_selected">
                            <img src={Spiderman} alt="" />
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    )
}
