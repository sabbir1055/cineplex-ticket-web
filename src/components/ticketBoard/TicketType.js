import React, {Fragment} from 'react'
import plus from '../../assets/images/icons/plus.svg'
import minus from '../../assets/images/icons/minus.svg'
export default function TicketType() {
    return (
        <Fragment>
            <div className="row">
                <div className="col-md-7">
                    <div className="ticket_booking_left_item seat_type">
                        <h3>Select Seat Type</h3>
                        <div className="card-wrap">
                            <div className="seat_type_select">
                                <div className="radio-primary">
                                    <input id="regular-cls" name="seatclassName" type="radio" />
                                    <label htmlFor="regular-cls">
                                        Regular
                                        <span>BDT 550.00</span>
                                    </label>
                                </div>
                                <div className="radio-primary">
                                    <input id="premium-cls" name="seatclassName" type="radio" />
                                    <label htmlFor="premium-cls">
                                        Premium
                                        <span>BDT 650.00</span>
                                    </label>
                                </div>
                                <div className="radio-primary">
                                    <input id="vip-cls" name="seatclassName" type="radio" />
                                    <label htmlFor="vip-cls">
                                        VIP
                                        <span>BDT 1250.00</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-5">
                    <div className="ticket_booking_left_item ticket_qty">
                        <h3>Ticket Quantity</h3>
                        <div className="card-wrap">
                            <div className="ticket_qty_view">
                                <div>
                                    <img src={minus} alt="" />
                                </div>
                                <div style={{"paddingTop":"6px"}}>
                                    <h4><small>0</small> Tickets</h4>
                                    <span>Max 4 Tickets</span>
                                </div>
                                <div>
                                    <img src={plus} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
