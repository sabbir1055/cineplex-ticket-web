import React, { Fragment } from 'react'

export default function MobileView() {
    return (
        <Fragment>
            <div className="row hidden-sm hidden-xs">
                <div className="col-md-12">
                    <div className="ticket_booking_left_item ticket_plan">
                        <h3>Select Seats</h3>
                        <div className="card-wrap">
                            <div className="ticket_cat_status">
                                <div className="ticket_cat">
                                    <ul>
                                        <li className="ticket_cat_reg">
                                            Regular
                                                        </li>
                                        <li className="ticket_cat_pre">
                                            Premium
                                                        </li>
                                        <li className="ticket_cat_vip">
                                            VIP
                                                        </li>
                                    </ul>
                                </div>
                                <div className="ticket_status">
                                    <ul>
                                        <li className="ticket_status_ok">
                                            Available
                                                        </li>
                                        <li className="ticket_status_select">
                                            Selected
                                                        </li>
                                        <li className="ticket_status_booked">
                                            Booked
                                                        </li>
                                        <li className="ticket_status_disable">
                                            Disabled
                                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <div className="seat-plan">
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="selected" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="selected" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>
                                        <a className="vip" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="premium" href="#">N1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">L1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                                <ul className="">
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">K1</a>
                                    </li>
                                    <li>
                                        <a className="disabled" href="#">J1</a>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <a className="" href="#">H1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">G1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">F1</a>
                                    </li>
                                    <li>
                                        <a className="booked" href="#">E1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">D1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">C1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">B1</a>
                                    </li>
                                    <li>
                                        <a className="" href="#">A1</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="clearfix"></div>
                            <div className="projection-wall"></div>
                            <p className="screen_name">THEATER SCREEN</p>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
