import React, { Fragment, useEffect, useState } from 'react'
import Header from '../common/Header'
import Footer from '../common/Footer'
import Movie from './ticketBoard/MovieList'
import Hall from './ticketBoard/Hall'
import MobileView from './ticketBoard/MobileView'
import DatepickerImage from '../assets/images/icons/datepicker.svg'
import close from '../assets/images/close.svg'
import poster from '../assets/images/icons/poster.png'
import TicketType from './ticketBoard/TicketType'
import '../assets/js/custom'

export default function TicketBoard() {
    useEffect(() => {
        var $sticky_sidebar = $('#sidebar');
        if ($sticky_sidebar.length) {
            var stickySidebar2 = new StickySidebar('#sidebar', {
                containerSelector: false,
                resizeSensor: true,
                topSpacing: 15,
                bottomSpacing: 0
            });
        };
    }, [1])
    return (
        <Fragment>
            <Header />
            <div className="inner-page">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="ticket_booking_left">
                                <div className="ticket_booking_left_item theater_location">
                                    <h3>Location <i className="fa fa-chevron-circle-down"></i> </h3>
                                    <a href="#" data-toggle="modal" data-target="#location_modal">Bashundhara Shopping Mall, Panthapath</a>
                                </div>
                                <div className="clearfix"></div>
                                <div className="ticket_booking_left_item movie_show_date">
                                    <h3>Select Date</h3>
                                    <ul className="d-flex">
                                        <li className="flex-equal-width selected">
                                            <p>SUN</p>
                                            <span>15 <small>SEP</small></span>
                                        </li>
                                        <li className="flex-equal-width">
                                            <p>MON</p>
                                            <span>16 <small>SEP</small></span>
                                        </li>
                                        <li className="flex-equal-width">
                                            <p>MON</p>
                                            <span>16 <small>SEP</small></span>
                                        </li>
                                    </ul>
                                </div>
                                <div className="clearfix"></div>
                                <Movie />
                                <div className="clearfix"></div>
                                <Hall />
                                <div className="clearfix"></div>
                                <TicketType />
                                <div className="row hidden-lg hidden-md ">
                                    <div className="col-md-12">
                                        <div className="ticket_booking_left_item ticket_plan">
                                            <h3>Select Seats</h3>
                                            <div className="card-wrap responsive-seat-plan-wrap">
                                                <div className="ticket_cat_status">
                                                    <div className="ticket_cat">
                                                        <ul>
                                                            <li className="ticket_cat_reg">
                                                                Regular
                            </li>
                                                            <li className="ticket_cat_pre">
                                                                Premium
                            </li>
                                                            <li className="ticket_cat_vip">
                                                                VIP
                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="ticket_status">
                                                        <ul>
                                                            <li className="ticket_status_ok">
                                                                Available
                            </li>
                                                            <li className="ticket_status_select">
                                                                Selected
                            </li>
                                                            <li className="ticket_status_booked">
                                                                Booked
                            </li>
                                                            <li className="ticket_status_disable">
                                                                Disabled
                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="clearfix"></div>
                                                <p className="scroll-text">
                                                    &larr; Scroll Left & Right to View All Seats &rarr;
                                                </p>
                                                <div className="responsive-seat-plan">
                                                    <div className="seat-plan">
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="selected" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="selected" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>
                                                                <a className="vip" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="premium" href="#">N1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">L1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                        <ul className="">
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">K1</a>
                                                            </li>
                                                            <li>
                                                                <a className="disabled" href="#">J1</a>
                                                            </li>
                                                            <li>

                                                            </li>
                                                            <li>
                                                                <a className="" href="#">H1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">G1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">F1</a>
                                                            </li>
                                                            <li>
                                                                <a className="booked" href="#">E1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">D1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">C1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">B1</a>
                                                            </li>
                                                            <li>
                                                                <a className="" href="#">A1</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                    <div className="projection-wall"></div>
                                                    <p className="screen_name">THEATER SCREEN</p>
                                                </div>
                                                <div className="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="ticket_booking_right" id="sidebar">
                                <h3>Tickets Summary</h3>
                                <div className="card-wrap">
                                    <img src={poster} alt="" />
                                    <div className="movie-details-info">
                                        <span>2D</span>
                                        <h2>Spider-Man: Far From Home</h2>
                                        <p>Action, Thriller</p>
                                        <p>Duration - 1h 47m</p>
                                    </div>
                                    <div className="clearfix"></div>
                                    <ul>
                                        <li className="show_date">
                                            Show Date <span>Sun, 15 Sep</span>
                                        </li>
                                        <li className="hall_name">
                                            Hall Name <span>Hall 03</span>
                                        </li>
                                        <li className="show_time">
                                            Show Time <span>1:30 PM</span>
                                        </li>
                                        <li className="seat_type">
                                            Show Time <span>Regular</span>
                                        </li>
                                        <li className="ticket_qty">
                                            Ticket Quantity <span>03</span>
                                        </li>
                                        <li className="selected_seat">
                                            Selected Seat <span>G11, G12, G13</span>
                                        </li>
                                    </ul>
                                    <div className="clearfix"></div>
                                    <div className="ticket_for">
                                        <p>Ticket For</p>
                                        <div className="seat_type_select">
                                            <div className="radio-primary">
                                                <input id="my-ticket" name="ticktfor" type="radio" />
                                                <label htmlFor="my-ticket">
                                                    You
                                                </label>
                                            </div>
                                            <div className="radio-primary">
                                                <input id="other-ticket" name="ticktfor" type="radio" />
                                                <label htmlFor="other-ticket">
                                                    Others
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="other_ticket_form" id="other_ticket_form">
                                        <input type="text" className="form-control" name="" placeholder="Full Name" />
                                        <input type="text" className="form-control" name="" placeholder="Mobile Number" />
                                    </div>
                                    <button type="button" className="btn btn-primary btn-block" name="button">Purchase Ticket</button>
                                    <p className="copywrite">By clicking the Purchase Tickets you are accepting <a href="#">Terms & Conditions</a> of Star Cineplex.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <MobileView />
                </div>
            </div>

            <div className="modal fade location_modal" id="location_modal" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">
                                <img src={close} alt="" />
                            </button>
                            <h4 className="modal-title text-center">Select your Theatre</h4>
                        </div>
                        <div className="modal-body location-modal-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="location_list">
                                        <ul className="row">
                                            <li className="col-md-12">
                                                <a href="#">
                                                    Bashundhara Shopping Mall
                                                            <p>Level 8, Bashundhara City, 13/3 Ka, Panthapath, Tejgaon.</p>
                                                </a>
                                            </li>
                                            <li className="col-md-12">
                                                <a href="#">
                                                    Shimanto Shambhar
                                                            <p>Level-9, Shimanto Shambhar, Pilkhana, Dhanmondi-2.</p>
                                                </a>
                                            </li>
                                            <li className="col-md-12">
                                                <a href="#">
                                                    SKS Tower
                                                            <p>Level-3, SKS Tower, Mohakhali.</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}
