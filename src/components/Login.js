import React, { Fragment, useContext, useState } from 'react'
import Header from '../common/Header'
import Footer from '../common/Footer'
import {UserContext} from '../context/UserContext'
export default function Login(props) {
    const [ user, setUser ] = useContext(UserContext)
    const [formData, setFormData] = useState({
        'email': ' ',
        'password': ' '
    })
    const handleSubmit = (e) => {
        e.preventDefault();
        if (formData.email === 'cineplex@test.com' && formData.password === '12345678') {
            localStorage.setItem('userInfo', JSON.stringify({
                'name': 'Sabbir Hosain',
                'email': 'cineplex@test.com',
                'image': 'https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-1/p200x200/95832861_3742864255783511_2605102869971468288_o.jpg?_nc_cat=110&_nc_sid=7206a8&_nc_ohc=mafkOB9OeuIAX_weUka&_nc_ht=scontent.fdac24-1.fna&_nc_tp=6&oh=1b32b8873b4db245c9368dcdd968db8b&oe=5F173BCC',
                'msisdn': '01684815187',
                'gender': 'Male',
                'member_since': '22-12-2020 10:46 PM',
            }))
            setUser({
                'name': 'Sabbir Hosain',
                'email': 'cineplex@test.com',
                'image': 'https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-1/p200x200/95832861_3742864255783511_2605102869971468288_o.jpg?_nc_cat=110&_nc_sid=7206a8&_nc_ohc=mafkOB9OeuIAX_weUka&_nc_ht=scontent.fdac24-1.fna&_nc_tp=6&oh=1b32b8873b4db245c9368dcdd968db8b&oe=5F173BCC',
                'msisdn': '01684815187',
                'gender': 'Male',
                'member_since': '22-12-2020 10:46 PM',
            })
            props.history.push('/home')
        } else {
            alert('Invalid username or password')
        }
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
        })
    }

    return (
        <Fragment>
            <Header />
            <div className="inner-page">
                <div className="faq-page pt-75 pb-0">
                    <div className="blog-page">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div className="login-form">
                                        <h2>Login to CINETICKETS</h2>
                                        <form onSubmit={handleSubmit}>
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <div className="form-group">
                                                        <label htmlFor="">Email / Mobile / Username</label>
                                                        <input id="email" className="form-control form-mane" name="email" required type="text" value={formData.email} onChange={handleChange} />
                                                    </div>
                                                    <div className="form-group mb-0">
                                                        <label htmlFor="">Password</label>
                                                        <input id="password" className="form-control form-mane" name="password" required type="password" value={formData.password} onChange={handleChange}  />
                                                    </div>
                                                    <div className="forgat-pass">
                                                        <div className="remember-me">
                                                            <a href="#">Forgot your <span className="green">Password ?</span></a>
                                                        </div>
                                                    </div>
                                                    <div className="buttons">
                                                        <button type="submit" name="button" className="btn btn-button green-bg button-1 btn-block">Login</button>
                                                    </div>
                                                    <a className="register_now" href="register.php">Did you have an account yet? <span className="green">Register Now</span></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}
