import React, { Fragment } from 'react'
import Header from '../common/Header'
import Footer from '../common/Footer'
export default function Register() {
    return (
        <Fragment>
            <Header/>
            <div className="inner-page">
                <div className="faq-page pt-75 pb-0">

                    <div className="blog-page">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div className="login-form">
                                        <h2>Register to CINETICKETS</h2>
                                        <form action="">
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <div className="form-group">
                                                        <label htmlFor="">Full Name</label>
                                                        <input id="name" className="form-control form-mane" required="" type="text" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Email</label>
                                                        <input id="email" className="form-control form-mane" required="" type="text" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Password</label>
                                                        <input id="password" className="form-control form-mane" required="" type="password" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Confirm Password</label>
                                                        <input id="confirm_password" className="form-control form-mane" required="" type="password" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Mobile Number</label>
                                                        <input id="" className="form-control form-mane" required="" type="text" />
                                                    </div>
                                                    <div className="form-group">
                                                        {/* <img src="http://ticket.cineplexbd.com/common/img/captcha/1592391997.1459.jpg" alt="" /> */}
                                                        <input id="recapcha" className="form-control form-mane" required="" type="text" placeholder="Enter Captcha" />
                                                    </div>
                                                    <div className="buttons">
                                                        <button type="button" name="button" className="btn btn-button green-bg button-1 btn-block">Register</button>
                                                    </div>
                                                    <a className="register_now" href="login.php">Already have an account? <span className="green">Login Now</span></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment >
    )
}
