import React, { Fragment } from 'react'
import Header from '../common/Header'
import Footer from '../common/Footer'
import '../assets/css/not-found.css'
export default function NotFound() {
    return (
        <Fragment>
            <Header />
            <div className="inner-page">
                <div className="faq-page pt-75 pb-0">
                    <div className="blog-page">
                        <div className="container">
                            <div className="row">
                                <div id="notfound">
                                    <div class="notfound">
                                        <div class="notfound-404">
                                            <h3>Oops! Page not found</h3>
                                            <h1><span>4</span><span>0</span><span>4</span></h1>
                                        </div>
                                        <h2>we are sorry, but the page you requested was not found</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}
