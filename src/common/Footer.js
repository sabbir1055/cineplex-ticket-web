import React, { Fragment } from 'react'
import cineLogo from '../assets/images/cine_logo2.png'
export default function Footer() {
    return (
        <Fragment>
            <footer className="footer-section">
                <div className="footer-bg">
                    <div className="container">
                        <div className="row">

                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-width">
                                <div className="ft-widget">
                                    <img src={cineLogo} alt="logo" width="140px" />
                                    <div className="ft-content">
                                        <p>
                                            <strong>Show Motion Limited </strong> <br />
                                                    Level 8, Bashundhara City <br />
                                                13/3 Ka, Panthapath, Tejgaon<br />
                                                Dhaka 1205, Bangladesh.
                                            </p>
                                        <p>
                                            <strong>Phone Number </strong> <br />
                                                    (+88) 9138260, 9134098 <br />
                                                (+88) 9141332, 9140819
                                            </p>
                                        <p>
                                            <strong>Email Address </strong> <br />
                                            <a href="mailto:info@cineplexbd.com"> info@cineplexbd.com</a>
                                        </p>
                                        <div className="social-link">
                                            <ul>
                                                <li><a href="" className="ft-fb"><span className="fa fa-facebook"></span></a></li>
                                                <li><a href="" className="ft-twitter"><span className="fa fa-twitter"></span></a></li>
                                                <li><a href="" className="ft-pintarest"><span className="fa fa-pinterest"></span></a></li>
                                                <li><a href="" className="ft-youtube"><span className="fa fa-youtube"></span></a></li>
                                                <li><a href="" className="ft-linkedin"><span className="fa fa-linkedin"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-6 sm-width">
                                <div className="ft-widget">
                                    <h2><span>More at Cineplex</span></h2>
                                    <div className="ft-content">
                                        <ul>
                                            <li><a href="#">Contact Us</a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Contests</a></li>
                                            <li><a href="#">Cineplex Mobile</a></li>
                                            <li><a href="#">The Playdium Store</a></li>
                                            <li><a href="#">Cineplex Digital Networks</a></li>
                                            <li><a href="#">Cineplex Digital Solutions</a></li>
                                            <li><a href="#">Advertising Opportunities</a></li>
                                            <li><a href="#">E-Gift Cards</a></li>
                                            <li><a href="#">Cineplex Magazine</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-6 sm-width">
                                <div className="ft-widget">
                                    <h2><span>Theatre Information</span></h2>
                                    <div className="ft-content">
                                        <ul>
                                            <li><a href="#">Find a Theatre</a></li>
                                            <li><a href="#">Food and Drink</a></li>
                                            <li><a href="#">Book a party</a></li>
                                            <li><a href="#">Stars and Strollers</a></li>
                                            <li><a href="#">Accessibility Policy</a></li>
                                            <li><a href="#">Access 2 Program</a></li>
                                            <li><a href="#">Accessibility Policy</a></li>
                                            <li><a href="#">Corporate Meetings &amp; Screenings</a></li>
                                            <li><a href="#">School Events &amp; Screenings</a></li>
                                            <li><a href="#">Guest Code of Conduct</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-width">
                                <div className="ft-widget">
                                    <h2><span>Like Us On Facebook</span></h2>
                                    <div className="ft-content">
                                        <div className="fb-page fb_iframe_widget" data-href="https://www.facebook.com/MYCINEPLEX" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=&amp;container_width=233&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FMYCINEPLEX&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false"><span style={{ "verticalAlign": "bottom", "width": "300px", "height": "181px" }}><iframe name="f23f44dcc983df" width="1000px" height="1000px" title="fb:page Facebook Social Plugin" frameBorder="0" allowtransparency="true" allowFullScreen={true} scrolling="no" allow="encrypted-media" src="https://web.facebook.com/v3.3/plugins/page.php?adapt_container_width=true&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Df8f18f743095b4%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%253A8888%252Ff3d60c536f53a6c%26relation%3Dparent.parent&amp;container_width=233&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FMYCINEPLEX&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false" style={{ "border": " none", "visibility": "visible", "width": "233px", "height": "181px" }} className=""></iframe></span></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="copyright">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-md-7 col-sm-7 col-xs-12 sm-width">
                                <div className="copyright-text">
                                    <p className="text-left">Copyright© 2019 <a href="">Cineplex</a> . All Rights Reserved.</p>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-12 sm-width">
                                <div className="copyright-text">
                                    <p>Design &amp; Developed by <a href="">SSL Wirelesws</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div className="to-top" id="back-top">
                <i className="fa fa-angle-up"></i>
            </div>
        </Fragment>
    )
}
