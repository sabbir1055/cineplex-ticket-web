import React, { Fragment, useEffect, useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import cineLogo from '../assets/images/cine_logo2.png'
import {UserContext} from '../context/UserContext'
export default function Header() {
    const [user, setUser] = useContext(UserContext)
    const [open, setOpen] = useState(false)
    
    const signOut = () => {
        localStorage.removeItem('userInfo')
        setUser(null)
    }
    useEffect(() => {
        var $preloader = $('.preloader');
        $preloader.find('.preloader-lod').fadeOut();
        $preloader.delay(350).fadeOut('slow');
    }, [1])


    return (
        <Fragment>
            <div className='preloader'>
                <div className='preloader-lod'></div>
                <div className='preloader-lod'></div>
                <div className='preloader-lod'></div>
                <div className='preloader-loding'>Cineplex…</div>
                <div className='large-square'></div>
            </div>

            <header className="header-section">

                <div className="header-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-6 header-logos sm-width">
                                <div className="header-logo">
                                    <Link to={user ? "/home" : "/login"}>
                                        <img src={cineLogo} alt="logo" />
                                    </Link>
                                </div>
                            </div>
                            <div className="col-xs-6 header-logout-area sm-width">
                                <div className="header-search">
                                    {
                                        user ?
                                            <div className={ open ? "dropdown pull-right open" : 'dropdown pull-right'}>
                                                <button type="button" id="search-toggle" className="blue-bg" type="button" data-toggle="dropdown" onClick={()=>{setOpen(!open)}}>Abdullah Al Mahmud <span className="fa fa-chevron-circle-down"></span></button>
                                                <ul className="dropdown-menu">
                                                    <li><a href="#">Profile</a></li>
                                                    <li><a href="#">Change Password</a></li>
                                                    <li><a href="#" onClick={signOut}>Sign Out</a></li>
                                                </ul>
                                            </div>
                                            :
                                            <Fragment>
                                                <Link to='/login'><button type="button" id="search-toggle" className="blue-bg"><span className="fa fa-sign-in"></span> Login</button></Link>
                                                <Link to='/register'><button type="button" id="search-toggle" className="blue-bg"><span className="fa fa-sign-in"></span> Register</button></Link>
                                            </Fragment>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </header>
        </Fragment>
    )
}
