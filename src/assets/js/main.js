(function($) {
    "use strict";
    jQuery(document).ready(function($) {
        // $('[data-toggle="tooltip"]').tooltip();

        $('.panel-collapse').on('show.bs.collapse', function () {
            $(this).siblings('.panel-heading').addClass('active');
        }
        );
        $('.panel-collapse').on('hide.bs.collapse', function () {
            $(this).siblings('.panel-heading').removeClass('active');
        }
        );
        // $('.alert').alert()
        /*** Top To ****/
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 300) {
                $('#back-top').fadeIn();
            }
            else {
                $('#back-top').fadeOut();
            }
        }
        );
        $('#back-top').on('click', function() {
            $("html, body").animate( {
                scrollTop: 0
            }
            , 1000);
            return false;
        }
        );
        // preloader
        // var winObj=$( window), bodyObj=$( 'body'), headerObj=$( 'header');
        $(window).on('load',function(){
            var $preloader=$( '.preloader');
            $preloader.find( '.preloader-lod').fadeOut();
            $preloader.delay( 350).fadeOut( 'slow');
        });
    }
    );
}

(jQuery))
