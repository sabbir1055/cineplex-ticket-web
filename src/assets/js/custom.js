$(document).ready(function(){
    var $date_picker = $('#datetimepicker3');
    var $sticky_sidebar = $('#sidebar');
    if($sticky_sidebar.length){
      var stickySidebar2 = new StickySidebar('#sidebar', {
        containerSelector: false,
        resizeSensor: true,
        topSpacing: 15,
        bottomSpacing: 0
      });
    };
    if($date_picker.length) {
      $('#datetimepicker3').datetimepicker({
          format: 'DD/MM/YYYY',
          icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            previous: "fa fa-arrow-left",
            next: "fa fa-arrow-right"
          }
      });
    };
    $('input[type="radio"]').click(function() {
      if($(this).attr('id') == 'other-ticket') {
        $('#other_ticket_form').show();
      }
      else {
        $('#other_ticket_form').hide();
      }
     });
  });
  // $(window).on('load',function() {
  //   $('#location_modal').modal('show');
  // });