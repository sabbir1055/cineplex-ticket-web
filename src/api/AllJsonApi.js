export const showDates = [
    {
        "availableMovies": [
            {
                "actorsName": "Sayed Babu, Sunerah Binte Kamal, Josefine Lindegaard",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587435172.webp",
                "dimensionCategory": "2D",
                "directorName": "Taneem Rahman Angshu",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1139/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Drama",
                "movieId": 1139,
                "movieLang": "Bangla",
                "movieLength": "02 hours 30 minutes",
                "movieRating": "7.1",
                "movieSynopsis": "<p>In a small beach town in Bangladesh, fearless Ayesha confronts social prohibition and violent opposition from her poverty-ridden family to surf. Like few other youngsters, she and her best friend Sohel are trained by self-made Bangladeshi surfer, Amir. As this unusual surfing enthusiasm gets international attention from surfing community and documentary film makers, fund money generates jealousy, squabbles, and power tussles. While surfing brings newfound fame and glory to Sohel, it is the 'prohibited' love for surfing that brings forced marriage and a life of misery for Ayesha. After seeking an extravagant, reckless lifestyle in the capital city, derailed Sohel returns back to Cox's Bazar, where their passion for surfing reunites them and unleashes a new hope for surfing in the small beach town.</p>",
                "movieTitle": "No Dorai (2D)",
                "movieTrailer": "https://www.youtube.com/watch?v=9NwAD2Dje1s",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587468372.webp",
                "releaseDate": "2019-12-29"
            },
            {
                "actorsName": "Dwayne Johnson, Jack Black, Kevin Hart",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607612564.webp",
                "dimensionCategory": "3D",
                "directorName": "Jake Kasdan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1141/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "English,Action,Adventure",
                "movieId": 1141,
                "movieLang": "English",
                "movieLength": "02 hours 3 minutes",
                "movieRating": "7.0",
                "movieSynopsis": "<p>In Jumanji: The Next Level, the gang is back but the game has changed. As they return to rescue one of their own, the players will have to brave parts unknown from arid deserts to snowy mountains, to escape the world's most dangerous game.</p>",
                "movieTitle": "Jumanji: The Next Level (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=rBxcF-r9Ibs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607591086.webp",
                "releaseDate": "2019-12-13"
            },
            {
                "actorsName": "Kristen Stewart, T.J. Miller, Jessica Henwick",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303273204.webp",
                "dimensionCategory": "3D",
                "directorName": "William Eubank",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1148/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Drama",
                "movieId": 1148,
                "movieLang": "English",
                "movieLength": "1h 35min",
                "movieRating": "6.3",
                "movieSynopsis": "<p>A crew of aquatic researchers work to get to safety after an earthquake devastates their subterranean laboratory. But the crew has more than the ocean seabed to fear.</p>",
                "movieTitle": "Underwater (2D)",
                "movieTrailer": "https://www.imdb.com/title/tt5774060/videoplayer/vi1414249497?ref_=tt_ov_vi",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303172118.webp",
                "releaseDate": "2020-01-10"
            },
            {
                "actorsName": "Robert Downey Jr., Antonio Banderas, Michael Sheen",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392650804.webp",
                "dimensionCategory": "3D",
                "directorName": "Stephen Gaghan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1169/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Adventure,Comedy,Family",
                "movieId": 1169,
                "movieLang": "English",
                "movieLength": "1h 41min",
                "movieRating": "5.6",
                "movieSynopsis": "<p>A physician discovers that he can talk to animals.</p>",
                "movieTitle": "Dolittle (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=FEf412bSPLs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392642668.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Will Smith, Martin Lawrence, Vanessa Hudgens",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257799064.webp",
                "dimensionCategory": "2D",
                "directorName": "Adil El Arbi",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1170/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action, Comedy",
                "movieId": 1170,
                "movieLang": "English",
                "movieLength": "2 hour 4 minutes",
                "movieRating": "7.3",
                "movieSynopsis": "<p>The Bad Boys Mike Lowrey and Marcus Burnett are back together for one last ride in the highly anticipated Bad Boys for Life.</p>",
                "movieTitle": "Bad Boys for Life",
                "movieTrailer": "https://www.youtube.com/watch?v=jKCj3XuPG8M",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257787156.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Margot Robbie, Rosie Perez, Mary Elizabeth Winstead",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320913137.webp",
                "dimensionCategory": "2D",
                "directorName": "Cathy Yan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1174/showdate/2020-06-10",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Crime,Adventure",
                "movieId": 1174,
                "movieLength": "1h 49min",
                "movieRating": "6.6",
                "movieSynopsis": "<p>After splitting with the Joker, Harley Quinn joins superheroes Black Canary, Huntress and Renee Montoya to save a young girl from an evil crime lord.</p>",
                "movieTitle": "Birds of Prey",
                "movieTrailer": "https://www.youtube.com/watch?v=kGM4uYZzfu0",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320969019.webp",
                "releaseDate": "2020-02-07"
            }
        ],
        "locID": 1,
        "showDate": "2020-06-10"
    },
    {
        "availableMovies": [
            {
                "actorsName": "Sayed Babu, Sunerah Binte Kamal, Josefine Lindegaard",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587435172.webp",
                "dimensionCategory": "2D",
                "directorName": "Taneem Rahman Angshu",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1139/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Drama",
                "movieId": 1139,
                "movieLang": "Bangla",
                "movieLength": "02 hours 30 minutes",
                "movieRating": "7.1",
                "movieSynopsis": "<p>In a small beach town in Bangladesh, fearless Ayesha confronts social prohibition and violent opposition from her poverty-ridden family to surf. Like few other youngsters, she and her best friend Sohel are trained by self-made Bangladeshi surfer, Amir. As this unusual surfing enthusiasm gets international attention from surfing community and documentary film makers, fund money generates jealousy, squabbles, and power tussles. While surfing brings newfound fame and glory to Sohel, it is the 'prohibited' love for surfing that brings forced marriage and a life of misery for Ayesha. After seeking an extravagant, reckless lifestyle in the capital city, derailed Sohel returns back to Cox's Bazar, where their passion for surfing reunites them and unleashes a new hope for surfing in the small beach town.</p>",
                "movieTitle": "No Dorai (2D)",
                "movieTrailer": "https://www.youtube.com/watch?v=9NwAD2Dje1s",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587468372.webp",
                "releaseDate": "2019-12-29"
            },
            {
                "actorsName": "Dwayne Johnson, Jack Black, Kevin Hart",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607612564.webp",
                "dimensionCategory": "3D",
                "directorName": "Jake Kasdan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1141/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "English,Action,Adventure",
                "movieId": 1141,
                "movieLang": "English",
                "movieLength": "02 hours 3 minutes",
                "movieRating": "7.0",
                "movieSynopsis": "<p>In Jumanji: The Next Level, the gang is back but the game has changed. As they return to rescue one of their own, the players will have to brave parts unknown from arid deserts to snowy mountains, to escape the world's most dangerous game.</p>",
                "movieTitle": "Jumanji: The Next Level (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=rBxcF-r9Ibs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607591086.webp",
                "releaseDate": "2019-12-13"
            },
            {
                "actorsName": "Kristen Stewart, T.J. Miller, Jessica Henwick",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303273204.webp",
                "dimensionCategory": "3D",
                "directorName": "William Eubank",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1148/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Drama",
                "movieId": 1148,
                "movieLang": "English",
                "movieLength": "1h 35min",
                "movieRating": "6.3",
                "movieSynopsis": "<p>A crew of aquatic researchers work to get to safety after an earthquake devastates their subterranean laboratory. But the crew has more than the ocean seabed to fear.</p>",
                "movieTitle": "Underwater (2D)",
                "movieTrailer": "https://www.imdb.com/title/tt5774060/videoplayer/vi1414249497?ref_=tt_ov_vi",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303172118.webp",
                "releaseDate": "2020-01-10"
            },
            {
                "actorsName": "Robert Downey Jr., Antonio Banderas, Michael Sheen",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392650804.webp",
                "dimensionCategory": "3D",
                "directorName": "Stephen Gaghan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1169/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Adventure,Comedy,Family",
                "movieId": 1169,
                "movieLang": "English",
                "movieLength": "1h 41min",
                "movieRating": "5.6",
                "movieSynopsis": "<p>A physician discovers that he can talk to animals.</p>",
                "movieTitle": "Dolittle (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=FEf412bSPLs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392642668.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Will Smith, Martin Lawrence, Vanessa Hudgens",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257799064.webp",
                "dimensionCategory": "2D",
                "directorName": "Adil El Arbi",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1170/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action, Comedy",
                "movieId": 1170,
                "movieLang": "English",
                "movieLength": "2 hour 4 minutes",
                "movieRating": "7.3",
                "movieSynopsis": "<p>The Bad Boys Mike Lowrey and Marcus Burnett are back together for one last ride in the highly anticipated Bad Boys for Life.</p>",
                "movieTitle": "Bad Boys for Life",
                "movieTrailer": "https://www.youtube.com/watch?v=jKCj3XuPG8M",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257787156.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Margot Robbie, Rosie Perez, Mary Elizabeth Winstead",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320913137.webp",
                "dimensionCategory": "2D",
                "directorName": "Cathy Yan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1174/showdate/2020-06-11",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Crime,Adventure",
                "movieId": 1174,
                "movieLength": "1h 49min",
                "movieRating": "6.6",
                "movieSynopsis": "<p>After splitting with the Joker, Harley Quinn joins superheroes Black Canary, Huntress and Renee Montoya to save a young girl from an evil crime lord.</p>",
                "movieTitle": "Birds of Prey",
                "movieTrailer": "https://www.youtube.com/watch?v=kGM4uYZzfu0",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320969019.webp",
                "releaseDate": "2020-02-07"
            }
        ],
        "locID": 1,
        "showDate": "2020-06-11"
    },
    {
        "availableMovies": [
            {
                "actorsName": "Sayed Babu, Sunerah Binte Kamal, Josefine Lindegaard",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587435172.webp",
                "dimensionCategory": "2D",
                "directorName": "Taneem Rahman Angshu",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1139/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Drama",
                "movieId": 1139,
                "movieLang": "Bangla",
                "movieLength": "02 hours 30 minutes",
                "movieRating": "7.1",
                "movieSynopsis": "<p>In a small beach town in Bangladesh, fearless Ayesha confronts social prohibition and violent opposition from her poverty-ridden family to surf. Like few other youngsters, she and her best friend Sohel are trained by self-made Bangladeshi surfer, Amir. As this unusual surfing enthusiasm gets international attention from surfing community and documentary film makers, fund money generates jealousy, squabbles, and power tussles. While surfing brings newfound fame and glory to Sohel, it is the 'prohibited' love for surfing that brings forced marriage and a life of misery for Ayesha. After seeking an extravagant, reckless lifestyle in the capital city, derailed Sohel returns back to Cox's Bazar, where their passion for surfing reunites them and unleashes a new hope for surfing in the small beach town.</p>",
                "movieTitle": "No Dorai (2D)",
                "movieTrailer": "https://www.youtube.com/watch?v=9NwAD2Dje1s",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787587468372.webp",
                "releaseDate": "2019-12-29"
            },
            {
                "actorsName": "Dwayne Johnson, Jack Black, Kevin Hart",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607612564.webp",
                "dimensionCategory": "3D",
                "directorName": "Jake Kasdan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1141/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "English,Action,Adventure",
                "movieId": 1141,
                "movieLang": "English",
                "movieLength": "02 hours 3 minutes",
                "movieRating": "7.0",
                "movieSynopsis": "<p>In Jumanji: The Next Level, the gang is back but the game has changed. As they return to rescue one of their own, the players will have to brave parts unknown from arid deserts to snowy mountains, to escape the world's most dangerous game.</p>",
                "movieTitle": "Jumanji: The Next Level (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=rBxcF-r9Ibs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157787607591086.webp",
                "releaseDate": "2019-12-13"
            },
            {
                "actorsName": "Kristen Stewart, T.J. Miller, Jessica Henwick",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303273204.webp",
                "dimensionCategory": "3D",
                "directorName": "William Eubank",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1148/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Drama",
                "movieId": 1148,
                "movieLang": "English",
                "movieLength": "1h 35min",
                "movieRating": "6.3",
                "movieSynopsis": "<p>A crew of aquatic researchers work to get to safety after an earthquake devastates their subterranean laboratory. But the crew has more than the ocean seabed to fear.</p>",
                "movieTitle": "Underwater (2D)",
                "movieTrailer": "https://www.imdb.com/title/tt5774060/videoplayer/vi1414249497?ref_=tt_ov_vi",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157882303172118.webp",
                "releaseDate": "2020-01-10"
            },
            {
                "actorsName": "Robert Downey Jr., Antonio Banderas, Michael Sheen",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392650804.webp",
                "dimensionCategory": "3D",
                "directorName": "Stephen Gaghan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1169/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Adventure,Comedy,Family",
                "movieId": 1169,
                "movieLang": "English",
                "movieLength": "1h 41min",
                "movieRating": "5.6",
                "movieSynopsis": "<p>A physician discovers that he can talk to animals.</p>",
                "movieTitle": "Dolittle (3D)",
                "movieTrailer": "https://www.youtube.com/watch?v=FEf412bSPLs",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/157949392642668.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Will Smith, Martin Lawrence, Vanessa Hudgens",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257799064.webp",
                "dimensionCategory": "2D",
                "directorName": "Adil El Arbi",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1170/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action, Comedy",
                "movieId": 1170,
                "movieLang": "English",
                "movieLength": "2 hour 4 minutes",
                "movieRating": "7.3",
                "movieSynopsis": "<p>The Bad Boys Mike Lowrey and Marcus Burnett are back together for one last ride in the highly anticipated Bad Boys for Life.</p>",
                "movieTitle": "Bad Boys for Life",
                "movieTrailer": "https://www.youtube.com/watch?v=jKCj3XuPG8M",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158124257787156.webp",
                "releaseDate": "2020-01-17"
            },
            {
                "actorsName": "Margot Robbie, Rosie Perez, Mary Elizabeth Winstead",
                "coverImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320913137.webp",
                "dimensionCategory": "2D",
                "directorName": "Cathy Yan",
                "links": [
                    {
                        "link": "http://localhost:8080/cinerestservices/webapi/shows/1/movie/1174/showdate/2020-06-12",
                        "rel": "showtimes"
                    }
                ],
                "movieGener": "Action,Crime,Adventure",
                "movieId": 1174,
                "movieLength": "1h 49min",
                "movieRating": "6.6",
                "movieSynopsis": "<p>After splitting with the Joker, Harley Quinn joins superheroes Black Canary, Huntress and Renee Montoya to save a young girl from an evil crime lord.</p>",
                "movieTitle": "Birds of Prey",
                "movieTrailer": "https://www.youtube.com/watch?v=kGM4uYZzfu0",
                "profileImage": "http://cineplex.publicdemo.xyz/cineplex-admin/public/uploads/158183320969019.webp",
                "releaseDate": "2020-02-07"
            }
        ],
        "locID": 1,
        "showDate": "2020-06-12"
    }
]