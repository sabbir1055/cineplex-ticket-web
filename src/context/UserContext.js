import React, { useState, createContext } from 'react'

export const UserContext = createContext()

export const UserProvider = (props) => {
    const [user, setUser] = useState(localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null)
    const [ticketData, setTicketData] = useState({})
    return (
        <UserContext.Provider value={[user, setUser, ticketData, setTicketData]}>{props.children}</UserContext.Provider>
    )
}