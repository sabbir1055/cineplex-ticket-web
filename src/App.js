import React from 'react';
import Router from './router/Route'
import {UserProvider} from './context/UserContext'
function App() {
  const user = { name: 'Sabbir', loggedIn: true }
  return (
    <div className="App">
      <div id="fb-root"></div>
      <script async defer crossOrigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3"></script>
      <div className="main bg_white">
        <UserProvider>
          <Router />
        </UserProvider>
      </div>
    </div >
  );
}

export default App;
